﻿import java.util.Scanner;

class Fahrkartenautomat{

static boolean x = true;

    public static void main(String[] args)
    {
	while (x == true)	{
	
       double zuZahlenderBetrag;
       double rückgabebetrag;
       
       zuZahlenderBetrag = fahrkartenbestellungErfassen();
       
       rückgabebetrag = fahrkartenBezahlen(zuZahlenderBetrag);
       
       fahrkartenAusgeben();
       rueckgeldAusgeben(rückgabebetrag);
	}
    }
		
    public static double fahrkartenbestellungErfassen () {
    	
    	Scanner tastatur = new Scanner(System.in);
    	double zuZahlenderBetrag;
    	int anzahlFahrscheine;
    	
    	System.out.print("Wie viele Fahrscheine moechten Sie kaufen?:");
        anzahlFahrscheine = tastatur.nextInt(); //Anzahl auf byte begrenzen um Speicher zu sparen
        
        zuZahlenderBetrag = anzahlFahrscheine * 2.50; //Einfach Anzahl mit regulärem Preis multiplizieren
        System.out.printf("Zu zahlender Betrag (EURO): " + "%.2f", (zuZahlenderBetrag)); 
    	
        return zuZahlenderBetrag;
    	}	
    
    public static double fahrkartenBezahlen(double zuZahlenderBetrag) {
    	
    	Scanner tastatur = new Scanner(System.in);
    	double eingeworfeneMünze = 0;
    	double eingezahlterGesamtbetrag = 0;
 
   
        while(eingezahlterGesamtbetrag < zuZahlenderBetrag)
        {
    	System.out.printf("\nNoch zu zahlen (EURO): " + "%.2f", (zuZahlenderBetrag - eingezahlterGesamtbetrag));
 	    System.out.println("\nEingabe (mind. 5Ct, höchstens 2 Euro):");
 	    eingeworfeneMünze = tastatur.nextDouble();
        eingezahlterGesamtbetrag += eingeworfeneMünze;
    	
    	
        }
        
        return eingezahlterGesamtbetrag - zuZahlenderBetrag;
        
    	}
    
    
    public static void fahrkartenAusgeben() {
    
        System.out.println("\nFahrscheine werden ausgegeben");
        
        for (int i = 0; i < 8; i++)
        {
           System.out.print("=");
           try {
 			Thread.sleep(250);
 		} catch (InterruptedException e) {
 			// TODO Auto-generated catch block
 			e.printStackTrace();
 		  }
        }
    }
    	public static void rueckgeldAusgeben(double rückgabebetrag) {
    		
    		  if(rückgabebetrag > 0.0)
    	       {
    	    	   System.out.printf("\n\nDer Rückgabebetrag in Höhe von %.2f", rückgabebetrag);
    	    	   System.out.println(" Euro wird in folgenden Münzen ausgezahlt:");

    	           while(rückgabebetrag >= 2.0) // 2 EURO-Münzen
    	           {
    	        	  System.out.println("2 EURO");
    		          rückgabebetrag -= 2.0;
    	           }
    	           while(rückgabebetrag >= 1.0) // 1 EURO-Münzen
    	           {
    	        	  System.out.println("1 EURO");
    		          rückgabebetrag -= 1.0;
    	           }
    	           while(rückgabebetrag >= 0.5) // 50 CENT-Münzen
    	           {
    	        	  System.out.println("50 CENT");
    		          rückgabebetrag -= 0.5;
    	           }
    	           while(rückgabebetrag >= 0.2) // 20 CENT-Münzen
    	           {
    	        	  System.out.println("20 CENT");
    	 	          rückgabebetrag -= 0.2;
    	           }
    	           while(rückgabebetrag >= 0.1) // 10 CENT-Münzen
    	           {
    	        	  System.out.println("10 CENT");
    		          rückgabebetrag -= 0.1;
    	           }
    	           while(rückgabebetrag >= 0.05)// 5 CENT-Münzen
    	           {
    	        	  System.out.println("5 CENT");
    	 	          rückgabebetrag -= 0.05;
    	           }
    	       }    	      
    	       System.out.println("\nVergessen Sie nicht, Ihre Fahrscheine\n"+ //Anpassung auf Anzahl der Fahrscheine (if, else)
    	                      "vor Fahrtantritt entwerten zu lassen!\n"+
    	                          "Wir wünschen Ihnen eine gute Fahrt.\n");
    	}
}

 	    
    
    

